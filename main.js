function swapTiles(cell1,cell2) {
  var temp = document.getElementById(cell1).className;
  document.getElementById(cell1).className = document.getElementById(cell2).className;
  document.getElementById(cell2).className = temp;
}

function shuffle() {
//Bucles para acceder a cada celda de la cuadrícula 3x3
for (var row=1;row<=3;row++) { //Para cada fila de la cuadrícula de 3x3
   for (var column=1;column<=3;column++) { //Para cada columna de esta fila
  
    var row2=Math.floor(Math.random()*3 + 1); //Escoje una fila al azar
    var column2=Math.floor(Math.random()*3 + 1); //Escoje una columna al azar
     
    swapTiles("cell"+row+column,"cell"+row2+column2); //Cambia la apariencia de ambas celdas
  } 
} 
}

function clickTile(row,column) {
  var cell = document.getElementById("cell"+row+column);
  var tile = cell.className;
  if (tile!="tile9") { 
       //Comprobando si el cuadrado blanco a la derecha
       if (column<3) {
         if ( document.getElementById("cell"+row+(column+1)).className=="tile9") {
           swapTiles("cell"+row+column,"cell"+row+(column+1));
           return;
         }
       }
       //Comprobando si el cuadrado blanco a la izquierda
       if (column>1) {
         if ( document.getElementById("cell"+row+(column-1)).className=="tile9") {
           swapTiles("cell"+row+column,"cell"+row+(column-1));
           return;
         }
       }
         //Comprobando si el cuadrado blanco está arriba
       if (row>1) {
         if ( document.getElementById("cell"+(row-1)+column).className=="tile9") {
           swapTiles("cell"+row+column,"cell"+(row-1)+column);
           return;
         }
       }
       //Comprobando si el cuadrado blanco está debajo
       if (row<3) {
         if ( document.getElementById("cell"+(row+1)+column).className=="tile9") {
           swapTiles("cell"+row+column,"cell"+(row+1)+column);
           return;
         }
       } 
  }
  
}

//Numero de clicks


//Cronometro


